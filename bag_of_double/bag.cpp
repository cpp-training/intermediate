#include <memory>
#include <iostream>
#include <cassert>
#include <algorithm>

class bag_of_double
{

public:

    // bag_of_double() : size_() {}

    bag_of_double() : data_(nullptr), size_(0) {}


    ~bag_of_double() 
    {
    	delete[] data_;

    }

    bag_of_double(std::size_t n)
    {
    	data_ = new double[n];
    	size_ = n;
    }

    bag_of_double(bag_of_double const& other)
    {
    	size_ = other.size_;
    	data_ = new double[size_];
    	std::copy(other.data(),other.data()+size_,data_);
    }

    // bag_of_double(bag_of_double const&& other): data_(nullptr), size_(0)
    bag_of_double(bag_of_double && other): bag_of_double()
    {

    	std::swap(size_, other.size_);
    	std::swap(data_, other.data_);

    	// size_ = other.size_;
    	// other.size_ = 0;

    	// data_ = other.data_;
    	// other.data_ = nullptr;
    }

    // bag_of_double& operator=(bag_of_double&& other)
    // {
    // 	std::swap(size_, other.size_);
    // 	std::swap(data_, other.data_);

    //     return *this;
    // }

    void swap(bag_of_double& other)
    {
    	std::swap(size_, other.size_);
    	std::swap(data_, other.data_);
    }

    bag_of_double& operator=(bag_of_double&& other)
    {
    	bag_of_double tmp;
    	swap(tmp);
    	swap(other);

        return *this;
    }

    bag_of_double& operator=(bag_of_double const& other)
    {
    	if(this != &other)
    	{
    	  size_ = other.size_;
    	  data_ = new double[size_];
    	  std::copy(other.data(),other.data()+size_,data_);
        }

    	return *this;
    }


	std::size_t size() const { return size_;}
	double const*    data() const { return data_ ;}
	double*          data() { return data_ ;}

private:
	double * data_ ;
	std::size_t size_;
};




int main(int argc, char const *argv[])
{
	/* code */

    bag_of_double d;

    std::cout << d.size() << "\n" ;
    assert(d.size() == 0);

    bag_of_double e(5);
    assert(e.size() == 5);
    // for (std:size_t i=0;i<e.size();i++)
    // {
    //   e.data()[i] = ;
    // }

    bag_of_double f(e);
    assert(f.size() == 5);

    // for (std::size_t i = 0; i < f.size(); i++)
    // {
    // 	assert();
    // }


	return 0;
}


