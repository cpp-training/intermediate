void tail_func() {}

template<typename T, typename... Tail>
void tail_func(T head, Tail... tail);


template<typename T>
void head_func(T value);

