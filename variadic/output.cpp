#include <iostream>

#include "output.h"


template<typename T, typename... Tail>
void tail_func(T head, Tail... tail)
{
    head_func(head);
    tail_func(tail...);
}

template<typename T>
void head_func(T value)
{
    std::cout << value << " ";
}

int main()
{
    std::cout << "First example :" << std::endl;
    tail_func(51, 52, "hello");

    std::cout << std::endl << std::endl;
    std::cout << "Second example :" << std::endl;
    tail_func("De Gaule", "Pompidou", "Giscard", "Miterrand", 
        "Chirac", "Sarkosy", "Hollande", "Macron");

    std::cout << std::endl;

}

