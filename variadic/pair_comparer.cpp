#include <iostream>
#include <string>

void display_result(bool status, std::string const & argument);

template<typename T>
bool pair_comparer(T a) {
  return false;
}

template<typename T>
bool pair_comparer(T a, T b) {
  // In real-world code, we wouldn't compare floating point values like
  // this. It would make sense to specialize this function for floating
  // point types to use approximate comparison.
  return a == b;
}

template<typename T, typename... Args>
bool pair_comparer(T a, T b, Args... args) {
  return a == b && pair_comparer(args...);
}


int main()
{
    bool status ;

    status = pair_comparer(1.5, 1.5, 2, 2, 6, 6);
    display_result(status, "1.5, 1.5, 2, 2, 6, 6");

    status = pair_comparer(1.5, 1.5, 2, 2, 6, 6, 7);
    display_result(status, "1.5, 1.5, 2, 2, 6, 6, 7");

}


void display_result(bool status, std::string const & argument)
{
    std::string success_msg = "All pairs have equal arguments";

    std::string fail_msg = "At least one pair is made with NOT EQUAL args";

    std::cout << "Input argument :" << std::endl
              << argument << std::endl;

    if (status)
        std::cout << success_msg << std::endl;
    else
        std::cout << fail_msg << std::endl;
}

