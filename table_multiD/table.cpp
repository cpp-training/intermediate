#include <iostream>
#include <utility>
#include <vector>
#include <tuple>

template<typename T, std::size_t N>
struct table
{
  table() : dims_{} {}

  template<typename... Int>
  table(Int const&... ds_) : dims_{ds_...}
  {
    static_assert( sizeof...(Int) == N, "size mismatch");

    strides_[0] = 1;
    for(std::size_t i =1; i<dims_.size();++i)
    {
        strides_[i] = strides_[i-1] * dims_[i-1];
    }

    data_.resize(strides_.back()*dims_.back());
  }

  table(table&&) =  default;
  table& operator=(table&&) =  default;

  auto size() const { return data_.size(); }

  template<typename... Int> decltype(auto) operator()(Int const&... is) const
  {
    auto idx = std::tie(is...);
    return data_[ linearize(idx,std::make_index_sequence<sizeof...(Int)>()) ];
  }

  template<typename... Int> decltype(auto) operator()(Int const&... is)
  {
    auto idx = std::tie(is...);
    return data_[ linearize(idx,std::make_index_sequence<sizeof...(Int)>()) ];
  }

  auto data()       { return data_.data(); }
  auto data() const { return data_.data(); }

  protected:

  template<typename Indexes, std::size_t... Idx>
  std::ptrdiff_t linearize(Indexes const& idx, std::integer_sequence<std::size_t,Idx...> const&)
  {
    std::size_t i = 0;

    (void)(std::initializer_list<std::size_t>{ (i += std::get<Idx>(idx)*std::get<Idx>(strides_))...    });

    return i;
  }

  private:
  std::array<std::ptrdiff_t,N>      dims_;
  std::array<std::ptrdiff_t,N>      strides_;
  std::vector<T>                    data_;
};

template<typename T, std::size_t N>
struct view
{
  template<typename... Int>
  view(T const* v, Int const&... ds_) : dims_{ds_...}
  {
    static_assert( sizeof...(Int) == N, "size mismatch");

    strides_[0] = 1;
    for(std::size_t i =1; i<dims_.size();++i)
    {
        strides_[i] = strides_[i-1] * dims_[i-1];
    }

    data_ = v;
  }

  view(view&&) =  default;
  view& operator=(view&&) =  default;

  auto size() const { return data_.size(); }

  template<typename... Int> decltype(auto) operator()(Int const&... is) const
  {
    auto idx = std::tie(is...);
    return data_[ linearize(idx,std::make_index_sequence<sizeof...(Int)>()) ];
  }

  template<typename... Int> decltype(auto) operator()(Int const&... is)
  {
    auto idx = std::tie(is...);
    return data_[ linearize(idx,std::make_index_sequence<sizeof...(Int)>()) ];
  }

  auto data()       { return data_; }
  auto data() const { return data_; }

  protected:

  template<typename Indexes, std::size_t... Idx>
  std::ptrdiff_t linearize(Indexes const& idx, std::integer_sequence<std::size_t,Idx...> const&)
  {
    std::size_t i = 0;

    (void)(std::initializer_list<std::size_t>{ (i += std::get<Idx>(idx)*std::get<Idx>(strides_))...    });

    return i;
  }

  private:
  std::array<std::ptrdiff_t,N>      dims_;
  std::array<std::ptrdiff_t,N>      strides_;
  T const*                          data_;
};

int main()
{
  table<float,3> d(3,3,2);
  for(int i=0;i<d.size();++i)
    (d.data())[i] = 1+i;

  std::cout << d.size() << "\n\n";
  for(int k=0; k<2 ;++k)
  {
    for(int j=0; j<3 ;++j)
    {
      for(int i =0; i <3 ;++i)
        std::cout << d(i,j,k) << " ";
      std::cout <<"\n";
    }
    std::cout <<"\n";
  }

  view<float,2> vd(d.data(),9,2);

  for(int j=0; j<2 ;++j)
  {
    for(int i =0; i <9 ;++i)
      std::cout << vd(i,j) << " ";
    std::cout <<"\n";
  }
}
