#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

struct functor
{
    functor(int reference): m_iref(reference)
    { }

    bool operator()(int a) 
    {
        return a == m_iref;
    }
    private:
     int m_iref;
};

struct Bidule
{
    Bidule(int target): m_ival{target}
    {
    }
    
    bool checker(int a)
    {
        return a == m_ival;
    }

    int func(const int target)
    {
        
        std::vector<int> toto = {1,2,3,4,56,7};
        
        auto result1 = std::find_if(toto.cbegin(), toto.cend(), 
            [&target](auto a) -> bool { return a == target;});

        //std::find_if(toto.cbegin(), toto.cend(), &x::checker);
     
        auto mychecker = std::bind(&Bidule::checker, this, std::placeholders::_1);   
        auto result2 = std::find_if(toto.cbegin(), toto.cend(), mychecker);
        
        
        auto result3 = std::find_if(toto.cbegin(), toto.cend(), functor{target});
        
        
        if (result1 != std::end(toto)) {
            std::cout << "contains: " << target << '\n';
        } else {
            std::cout << "does not contain: " << target << '\n';
        }
        
        if (result2 != std::end(toto)) {
            std::cout << "contains: " << target << '\n';
        } else {
            std::cout << "does not contain: " << target << '\n';
        }

        if (result3 != std::end(toto)) {
            std::cout << "contains: " << target << '\n';
        } else {
            std::cout << "does not contain: " << target << '\n';
        }
        
        return 0;
    }

    private:
        int m_ival;
};

int main()
{
    int target = 56;
    
    Bidule a{target};
    a.func(target);
}

