#include <iostream>
#include <vector>

template<typename T>
struct has_size
{

    template<typename U>
    static auto check( int ) -> decltype( std::declval<U>().size()
    	                                , std::true_type() );

    // , size_t(std::declval<U>)

    template<typename U> static std::false_type check( ... ) ;

    //decltype( check<T>(0) ) ;

    using type = std::is_same<decltype( check<T>(0) ), std::true_type> ;

};

// template<typename C>
// auto size(C const& c) -> decltype(c.size())
// {
// 	return c.size();
// }

template<bool C, typename R> struct only_if;

template<typename R> struct only_if<true,R>
{
	using type = R;
};

template<typename C>
typename only_if< has_size<C>::type::value, std::size_t>::type size(C const& c)
{
	   return c.size();
}

template<typename C>
typename only_if< !has_size<C>::type::value, std::size_t>::type size(C const& )
{
	   return 1u;
}


struct foo
{
	void size() {};

};

int main()
{
	std::vector<double> n(8);
	float x;
	foo f;

	std::cout << std::boolalpha << has_size<float>::type() << "\n";
	std::cout << std::boolalpha << has_size<std::vector<double> >::type() << "\n";
	std::cout << std::boolalpha << has_size<std::vector<double> >::type() << "\n";

	std::cout << size(n) << "\n" ;
	std::cout << size(x) << "\n" ;
	std::cout << size(f) << "\n" ;

	return 0;
}

