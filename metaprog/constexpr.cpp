#include <iostream>
#include <array>



constexpr int64_t factv0(int64_t n)
{
	int64_t r=1;

	for(int64_t i=2;i<=n;i++)
		r *= i;

	return r;
}

constexpr double factorial(int64_t n)
{
	double r=1.;

	for(int32_t i=2;i<=n;i++)
		r *= i;

	return r;
}

constexpr int64_t binomial(int64_t n, int64_t k)
{
	int64_t coef=1;

	if(n<0 || k<0 || k>n) 
		coef = 0 ;
	else 
		coef = factorial(n) / factorial(k) / factorial(n-k) ;
		
	return coef;
}

constexpr int64_t ipow(int base, unsigned exp) 
{
	auto result = 1;
	for(unsigned i=0 ; i<exp; ++i) result *= base ;

	return result;
}

constexpr int64_t trinomial(int64_t n, int64_t k)
{
	int64_t coef=0;

	if(n<0)
	{
		return trinomial(-n, k);
	} else
	{
		for (int j = 0; j < n+1; j++)
		{
			coef += ipow(-1, j) * binomial(n, j) * binomial(2*n-2*j, n-k-j) ;
		}
	}
		
	return coef;
}


int main(int argc, char ** argv)
{
	//std::array<float, factorial(3)> x{1.,2.,3.,4.,5.,6.};

    int nb = 20 ;

    std::cout << "Binomial triangle" << std::endl ;
    for(int n=0 ; n<nb+1 ; n++)
    {
        std::cout << "[" ;
	    for(int k=0 ; k<n+1 ; k++)
		{
			std::cout << binomial(n, k) << ", " ;
		}
		std::cout << "]" << std::endl ;
	}

    std::cout << "Trinomial triangle" << std::endl ;
    for(int n=0 ; n<nb+1 ; n++)
    {
        std::cout << "[" ;
	    for(int k=-n ; k<n+1 ; k++)
		{
			std::cout << trinomial(n, k) << ", " ;
		}
		std::cout << "]" << std::endl ;
	}



    return 0;
}

