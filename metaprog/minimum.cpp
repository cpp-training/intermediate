#include<iostream>
#include<vector>


template<typename R, typename... T>
using void_ = R ;

template<typename T>
void_<T, decltype(std::declval<T>() <std::declval<T>())> minimum(T a, T b)
{
	return a<b ? a : b;
}


int main()
{

	std::cout << minimum(4,3) << "\n";

	return 0;
}
